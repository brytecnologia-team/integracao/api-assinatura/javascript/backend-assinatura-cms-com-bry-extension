const request = require('request');

async function initializeSignature(req, res) {

    var fs = require('fs');

    var file = fs.createReadStream('./tmp/uploads/' + req.file.filename);

    const formInicialization = {
        nonce: req.body.nonce,
        attached: req.body.attached,
        profile: req.body.profile,
        hashAlgorithm: req.body.hashAlgorithm,
        certificate: req.body.certificate,
        operationType: req.body.operationType,
        'originalDocuments[0][nonce]': req.body.documentNonce,
        'originalDocuments[0][content]': file
    }

    const headers = {
        'Content-Type': 'multipart/form-data',
    };

    headers.Authorization = req.headers.authorization;

    const initializeSignature = () => {


        return new Promise((resolve, reject) => {

            request.post({ url: process.env.URL_INITIALIZATION, formData: formInicialization, headers: headers }, (error, response, body) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(body));
                }
            })
        })


    }

    await initializeSignature().then(result => {
        //console.log(result);
        console.log('');
        console.log('Initialization performed!');
        if (result.key) {
            console.log(result);
            return res.status(400).json({ error: result });
        } else {
            return res.json(result);
        }

    }).catch(error => {
        console.log(error);
        return res.status(500).json({ error: error });
    })

}

module.exports = initializeSignature;