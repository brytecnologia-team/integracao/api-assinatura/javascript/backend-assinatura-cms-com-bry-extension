const request = require('request');

async function finalizeSignature(req, res) {

    var fs = require('fs');

    var file = fs.createReadStream('./tmp/uploads/' + req.file.filename);

    const formFinalization = {
        nonce: req.body.nonce,
        attached: req.body.attached,
        profile: req.body.profile,
        hashAlgorithm: req.body.hashAlgorithm,
        certificate: req.body.certificate,
        operationType: req.body.operationType,
        'finalizations[0][nonce]': req.body.finalizationNonce,
        'finalizations[0][signedAttributes]': req.body.finalizationSignedAttributes,
        'finalizations[0][signatureValue]': req.body.finalizationSignatureValue,
        'finalizations[0][document]': file,

    }

    const headers = {
        'Content-Type': 'multipart/form-data',
    };

    headers.Authorization = req.headers.authorization;

    const finalizeSignature = () => {


        return new Promise((resolve, reject) => {

            request.post({ url: process.env.URL_FINALIZATION, formData: formFinalization, headers: headers }, (error, response, body) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(body));
                }
            })
        })


    }

    await finalizeSignature().then(result => {
        //console.log(result);
        console.log('Finalization performed!');
        if (result.key) {
            console.log(result);
            return res.status(400).json({ error: result });
        } else {
            return res.json(result);
        }
    }).catch(error => {
        console.log(error);
        return res.status(500).json({ error: error });
    })

};


module.exports = finalizeSignature;