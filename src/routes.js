const { Router } = require('express');
const multer = require('multer');
const multerConfig = require('./config/multer');


const initializeSignature = require('./app/controllers/InicializationController');

const finalizeSignature = require('./app/controllers/FinalizationController');

const routes = new Router();

//const storage = multer.memoryStorage();
//const upload = multer({ storage : storage });
const upload = multer(multerConfig);

routes.post('/initialize', upload.single('file'), function(req, res) { initializeSignature(req, res) });
routes.post('/finalize', upload.single('file'), function(req, res) { finalizeSignature(req, res) });

module.exports = routes;