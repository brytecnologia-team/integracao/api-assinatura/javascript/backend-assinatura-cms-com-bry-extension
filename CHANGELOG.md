# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
 
Empty

## [1.0.1] - 2020-04-27

### Changed

- Imports according to CommonJS specification.

### Removed 

- Nodemon configuration.

## [1.0.0] - 2020-03-27

### Added

- Examples of signature generation. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/frontend-assinatura-cms-com-bry-extension/-/tags/1.0.0
[1.0.1]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/frontend-assinatura-cms-com-bry-extension/-/tags/1.0.1