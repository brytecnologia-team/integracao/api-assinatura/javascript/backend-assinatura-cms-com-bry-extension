# Geração de Assinatura CMS com BRy Extension - Backend

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia JavaScript para geração de assinatura CMS. 

Este exemplo é integrado com o [frontend] de geração de assinatura. 

A aplicação realiza comunicação com o serviço de assinatura do Framework.

### Tech

O exemplo utiliza a biblioteca JavaScript abaixo:
* [Request] - Simplified HTTP request client
* [Dotenv] - Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env
* [Express] - Fast, unopinionated, minimalist web framework for node
* [Multer] - Multer is a node.js middleware for handling multipart/form-data, which is primarily used for uploading files
* [Streambuffers] - Buffer-based readable and writable streams

<b>1</b> - As dependências podem ser instaladas utilizando o gerenciador de pacotes npm ou yarn.

Na pasta raiz do projeto, execute o comando de instalação conforme o gerenciador de dependências de sua preferência:

Instalando as dependências com npm:

    npm install

Instalando a dependência com yarn:

    yarn

<b>2</b> - Para executar a aplicação execute o comando:

Com npm:

    npm run dev
 
Com yarn:

    yarn dev

   [Request]: <https://github.com/request/request>
   [Dotenv]: <https://github.com/motdotla/dotenv>
   [Express]: <https://github.com/expressjs/express>
   [Multer]: <https://github.com/expressjs/multer>
   [Streambuffers]: <https://github.com/jsantell/streambuffers>
   [frontend]: <https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/frontend-assinatura-cms-com-bry-extension>
   